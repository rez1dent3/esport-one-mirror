function cap() {
$('input#cap').style.background = "url(img.php?r=$rnd) no-repeat";
};

$('input#cap').keyup(function() { 
    var regV = /[A-Za-z0-9]{4}/; 
    var rRegV = $(this).val().search(regV); 
    if(rRegV == -1 && $(this).val().length > 0) {
        $(this).addClass('formRed'); 
        $(this).removeClass('formGreen'); 
    } else if($(this).val().length > 4){
        $(this).addClass('formRed'); 
        $(this).removeClass('formGreen');
    } else if($(this).val().length > 0) { 
        $(this).addClass('formGreen'); 
        $(this).removeClass('formRed'); 
    } else { 
        $(this).removeClass('formRed formGreen'); 
    } 
});
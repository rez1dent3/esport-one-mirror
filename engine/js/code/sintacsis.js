$('input[type="text"]').keyup(function() { 
    var regV = /(\w{3,5}):\/\/((?:(?:(?:\w[\.\-\+]?){1,60})\w)+)\.(\w{2,6})/; 
    var rRegV = $(this).val().search(regV); 
    if(rRegV == -1 && $(this).val().length > 0) { 
        $(this).addClass('formRed'); 
        $(this).removeClass('formGreen'); 
    } 
    else if($(this).val().length > 0) { 
        $(this).addClass('formGreen'); 
        $(this).removeClass('formRed'); 
    } 
    else { 
        $(this).removeClass('formRed formGreen'); 
    } 
});
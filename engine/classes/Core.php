﻿<?php

/**
 * @author REZ1DENT3
 * @copyright 2012
**/

defined('eSportSet') or exit('Доступ запрещен!');

spl_autoload_register ('LoadingOfFiles');
function LoadingOfFiles($LoadedClass) {
    $fileName = $LoadedClass.'.php';
    include_once ($fileName);
}

$AccessFile = new AccessFile;
$eSUpdate = new eSportUpdate;
$FFB = new pmgs;
$tpl = new tpl;
$ErrMySQL_Query = new query_MySQL;
$objMail = new mail;
$reg = new reg;

$AccessFile->call(Access,__FILE__);
$eSUpdate->call();
<!DOCTYPE HTML>
<html>
<head>
<title>{TITLE}</title>
<meta name="author" content="REZ1DENT3" />
<meta http-equiv="Content-Type" content="text/html; charset={MetaCharSet}" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link href="/css/main.css" rel="stylesheet" media="screen" />
<link href="/styles/shCore.css" rel="stylesheet" media="screen" />
<link href="/styles/shThemeDefault.css" rel="stylesheet" media="screen" />
<script type="text/javascript" src="/jquery/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="/jquery/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
</head>

<body>
{AUTH_LOGIN_OUT}
<div id="page">
  <aside id="sidebar">
    <nav><a class="logo" href="#top" id="nav-logo"><span>Портфолио</span>REZ1DENT3</a>
      <ul>
     	<li class="active" id="nav-1"><a href="/#home">Домой</a></li>
        <li id="nav-2"><a href="/news/#work">Новости</a></li>
        <li id="nav-3"><a href="/e/#cutthelink">Link Short</a></li>
        <li id="nav-4"><a href="/me/#about">Обо мне</a></li>
        <li id="nav-5"><a href="/#contact">Feedback</a></li>
      </ul>
      <div class="bg_bottom"></div>
    </nav>
  </aside>
  <div id="main-content">
  	<section id="top"></section>
    <section id="home">
      <div id="loader" class="loader"></div>
      <div id="ps_container" class="ps_container"> <span class="ribbon"></span>
        <div class="ps_image_wrapper">  
          <img src="{IMAGE_SITE_CONT}1.jpg" alt="" /> </div>
        <div class="ps_next"></div>
        <div class="ps_prev"></div>
        <ul class="ps_nav">
          <li class="selected"><a href="{IMAGE_SITE_CONT}1.jpg" rel="{IMAGE_SITE_CONT}thumbs/1.jpg">Image 1</a></li>
          <li><a href="{IMAGE_SITE_CONT}2.jpg" rel="{IMAGE_SITE_CONT}thumbs/2.jpg">Image 2</a></li>
          <li><a href="{IMAGE_SITE_CONT}3.jpg" rel="{IMAGE_SITE_CONT}thumbs/3.jpg">Image 3</a></li>
          <li><a href="{IMAGE_SITE_CONT}4.jpg" rel="{IMAGE_SITE_CONT}thumbs/4.jpg">Image 4</a></li>
          <li><a href="{IMAGE_SITE_CONT}5.jpg" rel="{IMAGE_SITE_CONT}thumbs/5.jpg">Image 5</a></li>
          <li class="ps_preview">
            <div class="ps_preview_wrapper"></div>
            <span></span> </li>
        </ul>
      </div>
      <header class="divider intro-text">
        <h2>{Site_Name} </h2>
      </header>
      <div class="my-services columns">
     
        <h3>CMS eSport {eSportVersion}:</h3>
     
        {__INFO_ALL__}
     
      </div>
      <!-- ком-рии
      <div class="one-column columns testimony">
      	<div class="clearfix says">
        <figure class="marginRight"><img src="images/client_image_1.jpg" alt="Image" /></figure>
        <blockquote>
          <p>This is just a place holder for people who need type to visualize what the actual copy might look like if it were real content.This is just a place holder for people who need type to visualize what the actual copy might look like if it were real content.</p>
          <cite>&mdash; <a href="#">Joefrey Mahusay</a></cite> </blockquote>
        </div>
        <div class="clearfix says">
        <figure class="marginRight"><img src="images/client_image_1.jpg" alt="Image" /></figure>
        <blockquote>
          <p>This is just a place holder for people who need type to visualize what the actual copy might look like if it were real content.This is just a place holder for people who need type to visualize what the actual copy might look like if it were real content.</p>
          <cite>&mdash; <a href="#">Joefrey Mahusay</a></cite> </blockquote>
        </div>
          
      </div>
      -->
    </section>
    
    <section id="work" class="clearfix">
      <header>
        <h2>Новости</h2>
      </header>
      <ul class="projects list">
      
        <li class="last">
        <b>{$row['title']}</b>
            <figure>
                <a href="{IMAGE_SITE_CONT}{img_full}" rel="work"><img alt="{INFO}" src="{IMAGE_SITE_CONT}work_img.jpg" /></a>
                <figcaption><a href="{LINK_NEWS}">Просмотреть<span>&nbsp;&rarr;</span></a></figcaption>
            </figure>
        </li>
             
      </ul>
    </section>
    
    <section id="cutthelink" class="clearfix">
      <header>
        <h2>Сократим ссылку.</h2>
      </header>
      {CUT_THE_LINK}
    </section>
    
    <section id="about" class="clearfix">
      {ABOUT}        
    </section>
    
    <section id="contact" class="clearfix">
      <header>
        <h2>Feedback</h2>
      </header>
      
      <form action="#contact" method="post">
      	<p><input type="text" name="name"  value="Ваше имя" id="name" onblur="if (this.value == ''){this.value = 'Ваше имя'; }" onfocus="if (this.value == 'Ваше имя') {this.value = '';}" /></p>
        <p><input type="text" name="email" value="Ваша почта" id="email"  onblur="if (this.value == ''){this.value = 'Ваша почта'; }" onfocus="if (this.value == 'Ваша почта') {this.value = '';}" /></p>
        <p><textarea cols="20" rows="7" name="message" id="message" onblur="if (this.value == ''){this.value = 'Ваше сообщение'; }" onfocus="if (this.value == 'Ваше сообщение') {this.value = '';}" >Ваше сообщение</textarea></p>
        {CAPTCHA}
        <!--p><input onclick='STPost()' type="submit" name="submit" value="Отправить" class="button" /></p-->
		<a href="#contact" onclick="STPost()">Отправить</a>
      </form>
      
      <div class="copyright">
      	<p><small>{COUNTER_SITES}</small></p>
      </div>
    </section>
  </div>
</div>

{___ALL_SCRIPT___}

</body>
</html>

<?php

/**
 * @author REZ1DENT3
 * @copyright 2012
**/

include_once ('/engine/data/define.php');
include_once (eSportClasses.	'Core.php');
include_once (eSportData.       'eSportSettings.php');

ob_start ();
session_start();
ob_implicit_flush (0);

(isset($_GET['version']))?exit(eSportVersion):null;
$db['default']['db_debug'] = developer;

error_reporting(developer?0:E_ALL & ~E_NOTICE);
date_default_timezone_set( "Europe/Moscow" );

ini_set('session.gc_maxlifetime',  120960);
ini_set('session.cookie_lifetime', 120960);
ini_set('display_errors',          developer);
ini_set('html_errors',             developer);
ini_set('session.save_path',       $_SERVER['DOCUMENT_ROOT'].eSport.'sessions/');

if (CHROME) {
   print file_get_contents(eSportTpl.'browser.tpl');
}

if (isset($_GET['pmgs'])){
    
	$in1 = array("name","e-mail","message","capt");
    $in2 = array("input","error","tr_g");
    $FFB->stand1 = array("Ваше имя","Ваша почта","Ваше сообщение","");
    
    $FFB->SetEncoding($in1[0],'имя!',                  1,CHROME);
    $FFB->SetEncoding($in1[1],'почту!',                1,CHROME);
    $FFB->SetEncoding($in1[2],'сообщение!',            1,CHROME);
    $FFB->SetEncoding($in1[3],'каптчу!',               1,CHROME);
    $FFB->SetEncoding($in2[0],'Введите ',              2,CHROME);
    $FFB->SetEncoding($in2[1],'Не верно введена ',     2,CHROME);
    $FFB->SetEncoding($in2[2],'Сообщение отправлено!', 2,CHROME);
    
	for($i=0;$i<count($in1);$i++) {
    	(isset($_POST[$in1[$i]])&&$_POST[$in1[$i]]!=$FFB->stand1[$i])?null:exit($FFB->stand['input'].$FFB->inf[$in1[$i]]);
	}

	((strtolower($_SESSION['cap']) != strtolower($_POST[$in1[3]])))?exit($FFB->stand['error'].$in1[$in1[3]]):null;
	(strlen($_POST[$in1[3]])!=4)?exit($FFB->stand['error'].$in1[$in1[3]]):null;
    
    $objMail->to = array('admin@gs-pc.ru');
    $objMail->from = $_POST[$in1[1]];
    $objMail->subject = 'Обратная связь - '.$_POST[$in1[0]];
    $objMail->body = $_POST[$in1[2]];
    //(isset($_GET['AddDOC']))?$objMail->add_attachment("Содержание атача. Предварительно нужно считать из файла", "название файла", "application/octet-stream"):null;
    $objMail->send();
    
    exit($FFB->stand['tr_g']);
    
}

if (isset($_GET['registration'])) {
    exit(md5(12345));   
}

if (isset($_GET['news'])){
    $title="Новости";
    $body=$row['text'];
} else {
    $title=$row['title'];
    $body='';//$row['information'];
}

$tpl->SetMainTpl(file_get_contents(eSportTpl."main.tpl"));
$tpl->SetPointer("/\{TITLE\}/",             $title);
$tpl->SetPointer("/\{BODY\}/",              $body);
$tpl->SetPointer("/\{CUT_THE_LINK\}/",      file_get_contents(eSportTpl."ctl.tpl"));
$tpl->SetPointer("/\{Site_Name\} /",        $_SERVER['SERVER_NAME']." PHP/MySQL/JS/JQ/HTML/CSS");
$tpl->SetPointer("/\{eSportVersion\}/",     eSportVersion);
$tpl->SetPointer("/\{MetaCharSet\}/",       ((CHROME)?'windows-1251':'utf-8'));
$tpl->SetPointer("/\{___ALL_SCRIPT___\}/",  file_get_contents(eSportTpl."AllScript.tpl"));
$tpl->SetPointer("/\{__INFO_ALL__\}/",      file_get_contents(eSportTpl.(isset($_GET['_reg'])?"Reg.tpl":"eSportINFO.tpl")));
$tpl->SetPointer("/\{COUNTER_SITES\}/",     file_get_contents(eSportTpl."counter.tpl"));
$tpl->SetPointer("/\{ABOUT\}/",             file_get_contents(eSportTpl."ABOUT.tpl"));
$tpl->SetPointer("/\{AUTH_LOGIN_OUT\}/",    file_get_contents(eSportTpl."AuthLoginOut.tpl"));
$tpl->SetPointer("/\{CAPTCHA\}/",           file_get_contents(eSportTpl."captcha.tpl"));
$tpl->SetPointer("/\{INFO_BROWSER\}/",      ((CHROME)?'<p>Каптча в вашем браузере может обновлятся всего 1 раз.</p>':null));
$tpl->SetPointer("/\{IMAGE_SITE_CONT\}/",   eSportTpl."images/");
$tpl->SetPointer("/\{___IMAGES___\}/",      eSportTpl);

exit(preg_replace($tpl->MatchTpl,$tpl->ReplaceTpl,$tpl->main));
